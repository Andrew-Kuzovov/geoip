using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DataAccess.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace GeoIpService.Controllers
{
    public class DataController : Controller
    {
        public DataController(IGeoDatabase database)
        {
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        [HttpGet("/ip/location")]
        [Produces("application/json")]
        public IActionResult GetCoordinatesByIpAddress(string ip)
        {
            if (string.IsNullOrEmpty(ip))
            {
                return BadRequest();
            }

            IPAddress address;
            if (!IpAddressFormat.IsMatch(ip) 
                || !IPAddress.TryParse(ip, out address))
            {
                return BadRequest();
            }

            var coordinatesOption = _database.GetCoordinatesByIpAddress(address);
            return coordinatesOption.Match(Ok, () => (IActionResult)NotFound());
        }

        [HttpGet("/city/locations")]
        [Produces("application/json")]
        public IActionResult GetLocationsByCityName(string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return BadRequest();
            }

            var locations = _database.GetLocationsByCityName(city);
            return Ok(locations);
        }

        private readonly IGeoDatabase _database;

        private static readonly Regex IpAddressFormat =
            new Regex(@"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", RegexOptions.Compiled);
    }
}
