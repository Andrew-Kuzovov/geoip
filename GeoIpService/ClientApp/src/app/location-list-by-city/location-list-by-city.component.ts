import { Component, OnInit, Inject } from '@angular/core';
import { Http } from '@angular/http';

interface LocationInfo {
  country: string;
  region: string;
  postal: string;
  city: string;
  organization: string;
  latitude: number;
  longitude: number;
}

@Component({
  selector: 'app-location-list-by-city',
  templateUrl: './location-list-by-city.component.html',
  styleUrls: ['./location-list-by-city.component.css']
})
export class LocationListByCityComponent implements OnInit {
  private baseUrl: string;
  private httpClient: Http;
  requestInProgress: boolean;
  city: string;
  locations: LocationInfo[];
  isNotFound: boolean;
  isBadRequest: boolean;

  constructor(@Inject('BASE_URL')baseUrl: string, httpClient: Http) {
    this.baseUrl = baseUrl;
    this.httpClient = httpClient;
    this.requestInProgress = false;
    this.city = "";
    this.clearErrors();
    this.clearData();
  }

  ngOnInit() {
  }

  clearErrors() {
    this.isNotFound = false;
    this.isBadRequest = false;
  }

  clearData() {
    this.locations = [];
  }

  isCityValid(): boolean {
    return this.city != null && this.city.length > 0;
  }

  isDataValid(): boolean {
    return this.locations != null && this.locations.length > 0;
  }

  public onSearch($event) {
    $event.preventDefault();

    this.requestInProgress = true;

    this.clearErrors();
    this.clearData();
    
    this.httpClient.get(this.baseUrl + "city/locations?city=" + encodeURIComponent(this.city)).subscribe(
      data => {
        this.requestInProgress = false;
        var result = data.json();
        this.locations = result;
        if (this.locations.length === 0) {
          this.isNotFound = true;
        }
      },
      error => {
        this.requestInProgress = false;
        this.clearData();

        if (error.status === 400) {
          this.isBadRequest = true;
        } else if (error.status === 404) {
          this.isNotFound = true;
        }

        console.log("Request failed.", error)
      });
  }
}
