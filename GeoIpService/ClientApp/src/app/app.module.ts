import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LocationByIpAddressComponent } from './location-by-ip-address/location-by-ip-address.component';
import { LocationListByCityComponent } from './location-list-by-city/location-list-by-city.component';
import { AppRoutingModule } from './/app-routing.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'location-by-ip-address', component: LocationByIpAddressComponent },
  { path: 'location-list-by-city', component: LocationListByCityComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LocationByIpAddressComponent,
    LocationListByCityComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
