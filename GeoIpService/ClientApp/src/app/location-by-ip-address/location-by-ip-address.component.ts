import { Component, OnInit, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-location-by-ip-address',
  templateUrl: './location-by-ip-address.component.html',
  styleUrls: ['./location-by-ip-address.component.css']
})
export class LocationByIpAddressComponent implements OnInit {
  private baseUrl: string;
  private httpClient: Http;
  requestInProgress: boolean;
  ip: string;
  latitude: number;
  longitude: number;
  isNotFound: boolean;
  isBadRequest: boolean;

  constructor(@Inject('BASE_URL')baseUrl: string, httpClient: Http) {
    this.baseUrl = baseUrl;
    this.httpClient = httpClient;
    this.requestInProgress = false;
    this.ip = "";
    this.clearErrors();
    this.clearData();
  }

  ngOnInit() {
  }

  clearErrors() {
    this.isNotFound = false;
    this.isBadRequest = false;
  }

  clearData() {
    this.latitude = NaN;
    this.longitude = NaN;
  }

  isIpValid(): boolean {
    // TODO: validate by RegExp
    return this.ip != null && this.ip.length > 0;
  }

  isDataValid(): boolean {
    return !isNaN(this.latitude) && !isNaN(this.longitude);
  }

  public onSearch($event) {
    $event.preventDefault();

    this.requestInProgress = true;

    this.clearErrors();
    this.clearData();

    this.httpClient.get(this.baseUrl + "ip/location?ip=" + encodeURIComponent(this.ip)).subscribe(
      data => {
        this.requestInProgress = false;
        var result = data.json();        
        this.latitude = result.latitude;
        this.longitude = result.longitude;
      },
      error => {
        this.requestInProgress = false;
        this.clearData();

        if (error.status === 400) {
          this.isBadRequest = true;
        } else if (error.status === 404) {
          this.isNotFound = true;
        }

        console.log("Request failed.", error)
      });
  }
}
