import { LocationListByCityComponent } from './location-list-by-city/location-list-by-city.component';
import { LocationByIpAddressComponent } from './location-by-ip-address/location-by-ip-address.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@NgModule({
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
