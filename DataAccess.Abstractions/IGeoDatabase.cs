﻿using System.Collections.Generic;
using System.Net;
using Journalist.Options;

namespace DataAccess.Abstractions
{
    public interface IGeoDatabase
    {
        Option<Coordinates> GetCoordinatesByIpAddress(IPAddress address);

        IReadOnlyCollection<LocationInfo> GetLocationsByCityName(string cityName);
    }
}
