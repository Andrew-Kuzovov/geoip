﻿using System;

namespace DataAccess.Abstractions
{
    public class Coordinates
    {
        public Coordinates(float latitude, float longitude)
        {
            if (float.IsNaN(latitude) || float.IsInfinity(latitude))
            {
                throw new ArgumentException(nameof(latitude));
            }

            if (float.IsNaN(longitude) || float.IsInfinity(longitude))
            {
                throw new ArgumentException(nameof(latitude));
            }

            Latitude = latitude;
            Longitude = longitude;
        }

        public float Latitude { get; }
        public float Longitude { get; }
    }
}
