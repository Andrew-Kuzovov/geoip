﻿using Journalist.Options;

namespace DataAccess.Abstractions
{
    public class DatabaseParameters
    {
        public Option<string> FilePath { get; set; }
    }
}
