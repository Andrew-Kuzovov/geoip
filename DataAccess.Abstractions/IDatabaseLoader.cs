﻿
namespace DataAccess.Abstractions
{
    public interface IDatabaseLoader
    {
        IGeoDatabase Load(DatabaseParameters parameters);
    }
}
