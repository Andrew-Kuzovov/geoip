﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DataAccess.Abstractions;
using Journalist.Options;

namespace BinaryDatabase
{
    internal sealed class Database : IGeoDatabase
    {
        public Database(
            int version,
            string name,
            DateTime timestamp,
            byte[] data,
            int recordsCount,
            uint offsetLocations,
            IpRange[] orderedRanges,
            Location[] orderedLocations)
        {
            _version = version;
            _name = name;
            _timestamp = timestamp;
            _recordsCount = recordsCount;
            _data = data;
            _offsetLocations = offsetLocations;
            _orderedRanges = orderedRanges;
            _orderedLocations = orderedLocations;
        }

        public Option<Coordinates> GetCoordinatesByIpAddress(IPAddress address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address));
            }

            var addressAsUInt32 = BitConverter.ToUInt32(address.GetAddressBytes().Reverse().ToArray(), 0);
            var index = FindIpRangeIndexByIp(addressAsUInt32);
            if (index == -1)
            {
                return Option.None();
            }

            unsafe
            {
                fixed (byte* location = &_data[_offsetLocations + _orderedRanges[index].location_index * 96])
                {
                    return Option.Some(
                        new Coordinates(*(float*)(location + 88), *(float*)(location + 92)));
                }
            }
        }

        public IReadOnlyCollection<LocationInfo> GetLocationsByCityName(string cityName)
        {
            if (string.IsNullOrEmpty(cityName))
            {
                throw new ArgumentNullException(nameof(cityName));
            }

            return GetLocationsByCityNameInternal(cityName)
                .Select(location =>
                    new LocationInfo(
                        location.country,
                        location.region,
                        location.postal,
                        location.city,
                        location.organization,
                        location.latitude,
                        location.longitude))
                .ToArray();
        }

        /// <summary>
        /// Тут можно оптимизировать сохранив left и right
        /// </summary>
        private Location[] GetLocationsByCityNameInternal(string cityName)
        {
            var index = FindLocationIndexByCityName(cityName);
            if (index == -1)
            {
                return new Location[0];
            }

            var left = index - 1;
            while (left >= 0 && string.CompareOrdinal(_orderedLocations[left].city, _orderedLocations[index].city) == 0)
            {
                left--;
            }

            var right = index + 1;
            while (right < _recordsCount && string.CompareOrdinal(_orderedLocations[right].city, _orderedLocations[index].city) == 0)
            {
                right++;
            }

            var locations = new Location[right - (left + 1)];
            for (int i = left + 1, j = 0; i < right; i++, j++)
            {
                locations[j] = _orderedLocations[i];
            }

            return locations;
        }

        private int FindIpRangeIndexByIp(uint ip)
        {
            int from = 0, to = _orderedRanges.Length - 1;

            while (from <= to)
            {
                var middleIndex = from + (to - from) / 2;

                var range = _orderedRanges[middleIndex];
                if (ip >= range.ip_from && ip <= range.ip_to)
                {
                    return middleIndex;
                }

                if (ip < range.ip_from)
                {
                    to = middleIndex - 1;
                }
                else
                {
                    from = middleIndex + 1;
                }
            }

            return -1;
        }

        private int FindLocationIndexByCityName(string cityName)
        {
            int from = 0, to = _orderedLocations.Length - 1;

            while (from <= to)
            {
                var middleIndex = from + (to - from) / 2;

                var location = _orderedLocations[middleIndex];
                if (string.CompareOrdinal(location.city, cityName) == 0)
                {
                    return middleIndex;
                }

                if (string.CompareOrdinal(cityName, location.city) < 0)
                {
                    to = middleIndex - 1;
                }
                else
                {
                    from = middleIndex + 1;
                }
            }

            return -1;
        }

        public override string ToString()
        {
            return $"{_name} v{_version} from {_timestamp:yyy-MM-dd HH:mm:ss}";
        }

        private readonly int _version;
        private readonly string _name;
        private readonly DateTime _timestamp;
        private readonly int _recordsCount;
        private readonly byte[] _data;
        private readonly uint _offsetLocations;
        private readonly IpRange[] _orderedRanges;
        private readonly Location[] _orderedLocations;
    }
}