﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using DataAccess.Abstractions;
using Journalist.Options;

namespace BinaryDatabase
{
    public class BinaryDatabaseLoader : IDatabaseLoader
    {
        public IGeoDatabase Load(DatabaseParameters parameters)
        {
            var dbFilePath = parameters.FilePath.GetOrThrow(
                () => new ArgumentException(
                    $"'{nameof(parameters.FilePath)}' parameter should be specified."));

            if (string.IsNullOrWhiteSpace(dbFilePath))
            {
                throw new ArgumentNullException(nameof(dbFilePath));
            }

            if (!File.Exists(dbFilePath))
            {
                throw new FileNotFoundException($"File '{dbFilePath}' not found.");
            }

            var data = File.ReadAllBytes(dbFilePath);

            using (var reader = new BinaryReader(
                new MemoryStream(data, false), Encoding.ASCII))
            {
                var version = reader.ReadInt32();
                var name = GetStringFromCharArray(reader.ReadChars(32));
                var timestamp = reader.ReadUInt64();
                var records = reader.ReadInt32();
                var offset_ranges = reader.ReadUInt32();
                var offset_cities = reader.ReadUInt32();
                var offset_locations = reader.ReadUInt32();

                if (reader.BaseStream.Position != offset_ranges)
                {
                    reader.BaseStream.Seek(offset_ranges, SeekOrigin.Begin);
                }

                var structSize = Marshal.SizeOf(typeof(IpRange));
                var rangesBytes = reader.ReadBytes(structSize * records);

                var orderedRanges = new IpRange[records];
                for (int offset = 0, i = 0; i < records; offset += structSize, i++)
                {
                    unsafe
                    {
                        fixed (byte* range = &rangesBytes[offset])
                        {
                            orderedRanges[i] = *(IpRange*)range;
                        }
                    }
                }

                var offsets_cities_ordered = new uint[records];
                Buffer.BlockCopy(data, (int)offset_cities, offsets_cities_ordered, 0, records * 4);

                if (reader.BaseStream.Position != offset_locations)
                {
                    reader.BaseStream.Seek(offset_locations, SeekOrigin.Begin);
                }

                var orderedLocations = new Location[records];

                // последовательная загрузка занимала не меньше 50-60ms
                var start = 0;
                var countDown = new CountdownEvent(Environment.ProcessorCount);
                foreach (var itemsToTake in DistributeIntegerEvenly(records, Environment.ProcessorCount))
                {
                    var from = start;
                    ThreadPool.UnsafeQueueUserWorkItem(_ =>
                    {
                        var to = from + itemsToTake;
                        for (var i = from; i < to; i++)
                        {
                            unsafe
                            {
                                fixed (byte* location = &data[offset_locations + offsets_cities_ordered[i]])
                                {
                                    orderedLocations[i].country = new string((sbyte*)location);
                                    orderedLocations[i].region = new string((sbyte*)(location + 8));
                                    orderedLocations[i].postal = new string((sbyte*)(location + 20));
                                    orderedLocations[i].city = new string((sbyte*)(location + 32));
                                    orderedLocations[i].organization = new string((sbyte*)(location + 56));
                                    orderedLocations[i].latitude = *(float*)(location + 88);
                                    orderedLocations[i].longitude = *(float*)(location + 92);
                                }
                            }
                        }

                        countDown.Signal();
                    }, null);

                    start += itemsToTake;
                }

                countDown.Wait();

                return new Database(
                    version,
                    name,
                    GetDateTimeFromUnixTimestamp(timestamp),
                    data,
                    records,
                    offset_locations,
                    orderedRanges,
                    orderedLocations);
            }
        }

        private static string GetStringFromCharArray(IEnumerable<char> chars)
        {
            var sb = new StringBuilder();
            foreach (var ch in chars)
            {
                if (ch == '\0')
                {
                    break;
                }

                sb.Append(ch);
            }

            return sb.ToString();
        }

        private static IEnumerable<int> DistributeIntegerEvenly(int total, int divider)
        {
            if (divider == 0)
            {
                yield return 0;
            }
            else
            {
                var rest = total % divider;
                var result = total / (double)divider;

                for (var i = 0; i < divider; i++)
                {
                    if (rest-- > 0)
                    {
                        yield return (int)Math.Ceiling(result);
                    }
                    else
                    {
                        yield return (int)Math.Floor(result);
                    }
                }
            }
        }

        private static DateTime GetDateTimeFromUnixTimestamp(double unixTimestamp)
        {
            var start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            start = start.AddSeconds(unixTimestamp);
            return start;
        }
    }
}
