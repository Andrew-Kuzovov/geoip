﻿using System;
using System.Net;

namespace BinaryDatabase
{
    internal struct IpRange
    {
        public uint ip_from;
        public uint ip_to;
        public uint location_index;

        public override string ToString()
        {
            var from = BitConverter.GetBytes(ip_from);
            var to = BitConverter.GetBytes(ip_to);

            Array.Reverse(from);
            Array.Reverse(to);

            return $"{new IPAddress(from)} - {new IPAddress(to)} [{ip_from}-{ip_to}]";
        }
    }
}